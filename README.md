Inhibitory dysfunction in a stochastic recurrent neural network which had been optimized for sampling-based inference
---------------------------------------------------------------------------------------------------------------------

This repository contains the code from `Echeveste et al., 2021`, to generate networks with inhibitory dysfunction.

In order to generate these matrices you can either:

1) [Use this colab online](https://colab.research.google.com/drive/1e7RPbGz3kE5fS4YUPxrsBgp9eyLPuWi-?usp=sharing)

2) Download the repo and run this Jupyter notebook locally `create_perturbed_Ws.ipynb`.


# Terms of use

This repository is provided for reproducibility purposes only. Please contact the authors if you wish to use any parts of this code for other purposes.



