import numpy as np
import scipy as sp
import methods as mt
from parameters import *
import matplotlib.pyplot as plt
import matplotlib as mpl
import datetime, os
from multiprocessing import Pool


# Function to estimate average baseline activity 

def estimate_mean_activity_level(W,h,mu_0,Sigma_0,Sigma_eta, sample_size = 400):
    t_bet_samp = 10 * tau_e
    steps_bet_samp = int(t_bet_samp/dt)
    
    transient = 25 * tau_e
    steps_transient = int(t_bet_samp/dt)
    
    u0 = np.random.multivariate_normal(mean = mu_0, cov = Sigma_0)
    eta0 = np.random.multivariate_normal(mean = np.zeros(N), cov = Sigma_eta)
    # We get rid of the transient
    u, eta = mt.network_evolution(W,h,u0,Sigma_eta, steps_max = steps_transient, eta = eta0);
    # And then we go on sampling
    (u_samples,_,_,_) = mt.network_sample(W,h,u,eta,sample_size,steps_bet_samp,Sigma_eta);

    mu_pop_av = np.mean(u_samples[:,:N_exc])
    
    return mu_pop_av
    
######################################
#   Defining function for 1 thread   #
######################################

def find_W_homeost(delta):
    sample_size = 25000
    
    location_old_params = "parameter_files/"
    out_location = "perturbed_W/"

    out_location_inhred_wh = out_location + "inh_red/with_homeo/"

    if not os.path.exists(out_location_inhred_wh):
        os.makedirs(out_location_inhred_wh)
        
    
    h_0 = np.loadtxt(location_old_params+"h_true_0_learn")
    mu_0 = np.loadtxt(location_old_params+"mu_evolved_net_0")
    Sigma_0 = np.loadtxt(location_old_params+"sigma_evolved_net_0")
    Sigma_eta = np.loadtxt(location_old_params+"sigma_eta_learn")
    
    mu_pop_target = np.mean(mu_0[:N_exc])
    
    W_orig = np.loadtxt(location_old_params+"w_learn")
    W_pert = np.copy(W_orig)
    W_pert[:,N_exc:] *= (1.-delta)
            
    tag = str(delta).replace(".","_")
           
    factor_array = np.logspace(-1, 1, num=11, endpoint=True, base=2) # effect sizes to explore
    
    n_cases_homeost = len(factor_array)
    
    W_candidates = np.empty([n_cases_homeost,N,N])
      
    error_homeost = np.empty([n_cases_homeost])
    
    for j, factor in enumerate(factor_array):
        delta_homeost = delta * factor
        print("Working with delta =", delta, " and trying delta_homeost =", delta_homeost)
        print("sampling...")
                
        W_candidates[j] = np.copy(W_pert)
    
        W_candidates[j,:,:N_exc] *= (1.-delta_homeost)
        
        J_pert = mt.get_Jacobian(W_candidates[j],mu_0)
        eivals_J_pert, _  = np.linalg.eig(J_pert)

        sr = np.amax(eivals_J_pert.real)

        if (sr > 0):
            stable = False
            error_homeost[j] = np.nan
            print("delta_homeost: ",delta_homeost, "is unstable.")
        else:
            mu_pop_av = estimate_mean_activity_level(W_candidates[j],h_0,mu_0,Sigma_0,Sigma_eta,sample_size=sample_size)
            error_homeost[j] = np.abs(mu_pop_target-mu_pop_av)
            print("delta_homeost",delta_homeost, "is stable. Error homeost:", error_homeost[j])
            
    print("errors: ", error_homeost)
    
    best_one = np.nanargmin(error_homeost)
    
    delta_homeost = delta * factor_array[best_one]
    
    print("Will use delta_homeost", delta_homeost)
    
    W_homeost = W_candidates[best_one]
    np.save(out_location_inhred_wh+"W_inh_red_"+tag+".npy",  W_homeost)
    np.savetxt(out_location_inhred_wh+"homeost_errors_inh_red_"+tag+".txt",  error_homeost)
    np.savetxt(out_location_inhred_wh+"delta_exc_vs_delta_inh_inh_red_"+tag+".txt",  np.column_stack((np.array([delta]),np.array([delta_homeost]))))

    
############
#   Main   #
############

def main():
    delta_array = np.array([0.0125,0.025,0.05,0.1,0.15,0.2]) # effect sizes to explore
    n_cases = len(delta_array)  
    
    # Send multiple threads to the pool
    with Pool(3) as p:
        p.map(find_W_homeost, delta_array)


# Call Main
if __name__ == "__main__":
    main()
